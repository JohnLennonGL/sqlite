package com.jlngls.bancodedadossqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {


            SQLiteDatabase bancoDados = openOrCreateDatabase("app", MODE_PRIVATE, null);

            // banco de dados
            bancoDados.execSQL("CREATE TABLE IF NOT EXISTS pessoa(nome VARCHAR, idade INT(3))");

           //bancoDados.execSQL("INSERT INTO pessoa(nome,idade) VALUES ('John', 23)");

            Cursor cursor = bancoDados.rawQuery("SELECT nome,idade FROM pessoa", null);


            int indiceColunaNome = cursor.getColumnIndex("nome");
            int indiceColunIdade = cursor.getColumnIndex("idade");

            cursor.moveToFirst();

            while (cursor != null) {

                Log.i("Resultado Nome ", cursor.getString(indiceColunaNome));
                Log.i("Resultado idade ", cursor.getString(indiceColunIdade));
                cursor.moveToNext();


            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}